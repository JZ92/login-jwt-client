import React, { useState, useEffect } from "react";
import styled from "styled-components/macro";
import axios from "axios";

const App = () => {
  const [isAuth, setIsAuth] = useState(false);
  const [user, setUser] = useState(null);
  const [sensitiveContent, setSensitiveContent] = useState("");

  useEffect(async () => {
    const token = window.localStorage.getItem("userToken");
    if (token) {
      // Parse the token and extract the user's id
      const userId = parseJwt(token).id;
      const url = `http://localhost:3030/api/users/${userId}`;
      const { data } = await axios.get(url);

      // Delete the password so it won't be displayed
      delete data.password;

      setUser(data);
      setIsAuth(true);
    }
  }, []);

  const parseJwt = (token) => {
    const base64Url = token.split(".")[1];
    const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    const jsonPayload = decodeURIComponent(
      atob(base64)
        .split("")
        .map(function (c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );
    return JSON.parse(jsonPayload);
  };

  const onLoginLogout = async () => {
    // Login
    if (!isAuth) {
      try {
        const hardCodedUser = { email: "sy@se.net", password: "0000" };
        const url = `http://localhost:3030/api/auth/login`;
        const { data } = await axios.post(url, hardCodedUser);

        if (data.user) {
          setIsAuth(true);
          // Delete the password so it won't be displayed
          delete data.user.password;
          setUser(data.user);
          window.localStorage.setItem("userToken", data.token);
        }
      } catch (err) {
        console.log("Error! ", err.message);
      }
    }
    // Logout
    else {
      setIsAuth(false);
      setUser(null);
      window.localStorage.removeItem("userToken");
    }
  };

  const getSensitiveContent = async () => {
    if (isAuth) {
      try {
        const options = {
          headers: {
            "x-access-token": window.localStorage.getItem("userToken"),
          },
        };
        const url = `http://localhost:3030/api/auth/me`;
        const { data } = await axios.get(url, options);

        if (data.message) setSensitiveContent(data.message);
      } catch (err) {
        console.log("Error! ", err.message);
      }
    }
  };

  return (
    <Wrapper>
      <Title>Auth demo</Title>
      <StatusDiv>Authenticated: {isAuth ? "true" : "false"}</StatusDiv>
      <StatusDiv>
        User: {""}
        {user ? (
          <div>
            {"{"}
            {Object.keys(user).map((key, i) => (
              <p key={i}>
                <Span>"{key}": </Span>
                <Span>"{user[key]}"</Span>
              </p>
            ))}
            {"}"}
          </div>
        ) : (
          "null"
        )}
      </StatusDiv>
      <ButtonsWrapper>
        <Button onClick={onLoginLogout}>{isAuth ? "Logout" : "Login"}</Button>
        <Button onClick={getSensitiveContent}>Get Protected Content</Button>
      </ButtonsWrapper>
      <StatusDiv>{sensitiveContent || "No content yet..."}</StatusDiv>
    </Wrapper>
  );
};

export default App;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
`;

const Title = styled.h1`
  font-size: 4rem;
  margin-bottom: 5rem;
`;

const StatusDiv = styled.div`
  font-size: 2rem;
  margin-bottom: 2rem;
`;

const Button = styled.button`
  border-radius: 10%;
  background-color: #80ddf2;
  color: #292c33;
  font-size: 2rem;
  cursor: pointer;
  padding: 1rem;
  border: none;
  font-weight: 600;
  :first-child {
    margin-right: 2rem;
  }
`;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  margin-bottom: 2rem;
`;

const Span = styled.span`
  margin-left: 2rem;
`;
