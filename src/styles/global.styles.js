import { createGlobalStyle } from "styled-components/macro";

const GlobalStyles = createGlobalStyle`
@import url("https://fonts.googleapis.com/css?family=Roboto:400,700");
    html,
    body {
      min-height: 100%;
    }
    html {
      font-size: 10px;
      height:100%
    }
    #root{
        height: 100%;
        width: 100%;
    }
    body {
      background-color: #292C33;
      display: flex;
      min-height: 100vh;
      justify-content: center;
      align-items: center;  
      font-family: "Roboto", sans-serif;
   
      /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    }
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
     
    }
    li {
      list-style-type: none;
    }
`;

export default GlobalStyles;
