import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import GlobalStyles from "./styles/global.styles.js";

const rootElement = document.getElementById("root");
ReactDOM.render(
  <>
    <App />
    <GlobalStyles />
  </>,
  rootElement
);
